package org.shobhank.modernart;

import android.support.v7.app.ActionBarActivity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final View rect1 = findViewById(R.id.rect1);
		final View rect2 = findViewById(R.id.rect2);
		final View rect3 = findViewById(R.id.rect3);
		final View rect4 = findViewById(R.id.rect4);
		final View rect5 = findViewById(R.id.rect5);

		ColorDrawable rect1Drawable = (ColorDrawable)rect1.getBackground();
		ColorDrawable rect2Drawable = (ColorDrawable)rect2.getBackground();
		ColorDrawable rect3Drawable = (ColorDrawable)rect3.getBackground();
		ColorDrawable rect4Drawable = (ColorDrawable)rect4.getBackground();
		ColorDrawable rect5Drawable = (ColorDrawable)rect5.getBackground();
		
		final int rect1Color = rect1Drawable.getColor();
		final int rect2Color = rect2Drawable.getColor();
		final int rect3Color = rect3Drawable.getColor();
		final int rect4Color = rect4Drawable.getColor();
		final int rect5Color = rect5Drawable.getColor();
		
		SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(rect1Color!=-1)
					rect1.setBackgroundColor(rect1Color*(progress+1));
				
				if(rect2Color!=-1)
					rect2.setBackgroundColor(rect2Color*(progress+1));
				
				if(rect3Color!=-1)
					rect3.setBackgroundColor(rect3Color*(progress+1));
				
				if(rect4Color!=-1)
					rect4.setBackgroundColor(rect4Color*(progress+1));
				
				if(rect5Color!=-1)
					rect5.setBackgroundColor(rect5Color*(progress+1));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.more_info) {
			MoreInformationDialog dialog = new MoreInformationDialog();
			dialog.show(getFragmentManager(), "More Information Dialog");
		}
		return super.onOptionsItemSelected(item);
	}
}
