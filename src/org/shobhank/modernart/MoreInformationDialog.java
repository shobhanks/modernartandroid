package org.shobhank.modernart;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MoreInformationDialog extends DialogFragment {
	    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_TRADITIONAL);
        builder.setMessage(R.string.more_info_str)
               .setPositiveButton(R.string.visit_moma, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.moma.org/"));
                	   startActivity(browserIntent);
                   }
               })
               .setNegativeButton(R.string.not_now, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                   }
               });
        return builder.create();
    }
}
